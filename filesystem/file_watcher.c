/**
 * @author   Казаков Андрей 
 * @date     20.02.19.
 */

#include <errno.h>
#include <string.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/inotify.h>
#include <unistd.h>

static void handle_events(int fd, const int* wd, int argc, char** argv)
{
	char buf[4096] __attribute__ ((aligned(__alignof__(struct inotify_event))));
	const struct inotify_event* event;
	int     i;
	ssize_t len;
	char* ptr;

	while(1)
	{
		len = read(fd, buf, sizeof buf);
		if(len == -1 && errno != EAGAIN)
		{
			perror("read");
			exit(EXIT_FAILURE);
		}

		if(len <= 0)
			break;

		for(ptr = buf; ptr < buf + len; ptr += sizeof(struct inotify_event) + event->len)
		{
			event = (const struct inotify_event*)ptr;

			if(event->mask & IN_ACCESS)
				printf("IN_ACCESS: ");
			else
				if(event->mask & IN_MODIFY)
					printf("IN_MODIFY: ");
				else
					if(event->mask & IN_ATTRIB)
						printf("IN_ATTRIB: ");
					else
						if(event->mask & IN_CLOSE_WRITE)
							printf("IN_CLOSE_WRITE: ");
						else
							if(event->mask & IN_CLOSE_NOWRITE)
								printf("IN_CLOSE_NOWRITE: ");
							else
								if(event->mask & IN_CLOSE)
									printf("IN_CLOSE: ");
								else
									if(event->mask & IN_OPEN)
										printf("IN_OPEN: ");
									else
										if(event->mask & IN_MOVED_FROM)
											printf("IN_MOVED_FROM: ");
										else
											if(event->mask & IN_MOVED_TO)
												printf("IN_MOVED_TO: ");
											else
												if(event->mask & IN_MOVE)
													printf("IN_MOVE: ");
												else
													if(event->mask & IN_CREATE)
														printf("IN_CREATE: ");
													else
														if(event->mask & IN_DELETE)
															printf("IN_DELETE: ");
														else
															if(event->mask & IN_DELETE_SELF)
																printf("IN_DELETE_SELF: ");
															else
																if(event->mask & IN_MOVE_SELF)
																	printf("IN_MOVE_SELF: ");

			for(i = 1; i < argc; ++i)
				if(wd[i] == event->wd)
				{
					printf("%s/", argv[i]);
					break;
				}

			if(event->len)
				printf("%s", event->name);

			if(event->mask & IN_ISDIR)
				printf(" [directory]\n");
			else
				printf(" [file]\n");
		}
	}
}

int main(int argc, char** argv)
{
	if(argc < 2)
	{
		printf("Usage: %s PATH [PATH ...]\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	char buf;
	int  fd, i, poll_num;
	int* wd;
	nfds_t        nfds;
	struct pollfd fds[2];

	printf("Press ENTER key to terminate.\n");

	fd = inotify_init1(IN_NONBLOCK);
	if(fd == -1)
	{
		printf("%s(%d)", strerror(errno), errno);
		exit(EXIT_FAILURE);
	}

	wd = (int*)calloc((size_t)argc, sizeof(int));
	if(wd == NULL)
	{
		printf("%s(%d)", strerror(errno), errno);
		exit(EXIT_FAILURE);
	}

	for(i = 1; i < argc; ++i)
	{
		wd[i] = inotify_add_watch(fd, argv[i], IN_ALL_EVENTS);
		if(wd[i] == -1)
		{
			printf("%s(%d)", strerror(errno), errno);
			exit(EXIT_FAILURE);
		}
	}

	nfds = 2;

	fds[0].fd     = STDIN_FILENO;
	fds[0].events = POLLIN;

	fds[1].fd     = fd;
	fds[1].events = POLLIN;

	printf("Listening for events.\n");
	while(1)
	{
		poll_num = poll(fds, nfds, -1);
		if(poll_num == -1)
		{
			if(errno == EINTR)
				continue;
			printf("%s(%d)", strerror(errno), errno);
			exit(EXIT_FAILURE);
		}

		if(poll_num > 0)
		{
			if(fds[0].revents & POLLIN)
			{
				while(read(STDIN_FILENO, &buf, 1) > 0 && buf != '\n')
					continue;
				break;
			}

			if(fds[1].revents & POLLIN)
				handle_events(fd, wd, argc, argv);
		}
	}

	printf("Listening for events stopped.\n");

	close(fd);
	free(wd);
}
