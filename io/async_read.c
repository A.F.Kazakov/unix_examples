/**
 * @author   Казаков Андрей 
 * @date     17.01.19.
 */

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <aio.h>

#define BUF_SIZE 20

#define err_exit(msg) do { perror(msg); exit(EXIT_FAILURE); } while (0)

#define err_msg(msg)  do { perror(msg); } while (0)

#define IO_SIGNAL SIGUSR1

struct io_request
{
	int request_num;
	int status;
	struct aiocb* aiocbp;
};

static volatile sig_atomic_t got_SIGQUIT = 0;

static void quit_handler(int sig) { got_SIGQUIT = 1; }

static void aio_signal_handler(int sig, siginfo_t* si, void* ucontext)
{
	printf("I/O completion signal received\n");
}

int main(int argc, char** argv)
{
	struct io_request* io_list;
	struct aiocb     * aio_cb_list;
	struct sigaction sa;
	int              s, j;
	size_t           number_reqs;
	size_t           open_reqs;

	if(argc < 2)
	{
		dprintf(STDERR_FILENO, "Usage: %s <pathname> <pathname>...\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	number_reqs = (size_t)(argc - 1);

	io_list = calloc(number_reqs, sizeof(struct io_request));
	if(io_list == NULL)
		err_exit("calloc");

	aio_cb_list = calloc(number_reqs, sizeof(struct aiocb));
	if(aio_cb_list == NULL)
		err_exit("calloc");

	sa.sa_flags = SA_RESTART;
	sigemptyset(&sa.sa_mask);

	sa.sa_handler = quit_handler;
	if(sigaction(SIGQUIT, &sa, NULL) == -1)
		err_exit("sigaction");

	sa.sa_flags     = SA_RESTART | SA_SIGINFO;
	sa.sa_sigaction = aio_signal_handler;
	if(sigaction(IO_SIGNAL, &sa, NULL) == -1)
		err_exit("sigaction");

	for(j = 0; j < number_reqs; ++j)
	{
		io_list[j].request_num = j;
		io_list[j].status      = EINPROGRESS;
		io_list[j].aiocbp      = &aio_cb_list[j];

		io_list[j].aiocbp->aio_fildes = open(argv[j + 1], O_RDONLY);
		if(io_list[j].aiocbp->aio_fildes == -1)
			err_exit("open");

		printf("opened %s on descriptor %d\n", argv[j + 1], io_list[j].aiocbp->aio_fildes);

		io_list[j].aiocbp->aio_buf = malloc(BUF_SIZE);
		if(io_list[j].aiocbp->aio_buf == NULL)
			err_exit("malloc");

		io_list[j].aiocbp->aio_nbytes                         = BUF_SIZE;
		io_list[j].aiocbp->aio_reqprio                        = 0;
		io_list[j].aiocbp->aio_offset                         = 0;
		io_list[j].aiocbp->aio_sigevent.sigev_notify          = SIGEV_SIGNAL;
		io_list[j].aiocbp->aio_sigevent.sigev_signo           = IO_SIGNAL;
		io_list[j].aiocbp->aio_sigevent.sigev_value.sival_ptr = &io_list[j];

		s = aio_read(io_list[j].aiocbp);
		if(s == -1)
			err_exit("aio_read");
	}

	open_reqs = number_reqs;

	while(open_reqs > 0)
	{
		sleep(3);

		if(got_SIGQUIT)
		{
			printf("got SIGQUIT; canceling I/O requests: \n");

			for(j = 0; j < number_reqs; ++j)
				if(io_list[j].status == EINPROGRESS)
				{
					printf("\tRequest %d on descriptor %d:", j, io_list[j].aiocbp->aio_fildes);
					s = aio_cancel(io_list[j].aiocbp->aio_fildes, io_list[j].aiocbp);
					if(s == AIO_CANCELED)
						printf("I/O canceled\n");
					else
						if(s == AIO_NOTCANCELED)
							printf("I/O not canceled\n");
						else
							if(s == AIO_ALLDONE)
								printf("I/O all done\n");
							else
								err_msg("aio_cancel");
				}

			got_SIGQUIT = 0;
		}

		printf("aio_error():\n");
		for(j = 0; j < number_reqs; ++j)
			if(io_list[j].status == EINPROGRESS)
			{
				printf("\tfor request %d (descriptor %d): ", j, io_list[j].aiocbp->aio_fildes);
				io_list[j].status = aio_error(io_list[j].aiocbp);

				switch(io_list[j].status)
				{
					case 0: printf("I/O succeeded\n");
						break;
					case EINPROGRESS: printf("In progress\n");
						break;
					case ECANCELED: printf("Canceled\n");
						break;
					default: err_msg("aio_error");
						break;
				}

				if(io_list[j].status != EINPROGRESS)
					--open_reqs;
			}
	}

	printf("All I/O requests completed\n");

	printf("aio_return():\n");
	for(j = 0; j < number_reqs; ++j)
	{
		ssize_t ret;

		ret = aio_return(io_list[j].aiocbp);
		printf("\tfor request %d (descriptor %d): %ld\n", j, io_list[j].aiocbp->aio_fildes, ret);
	}
}