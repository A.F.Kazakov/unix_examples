/**
 * @author   Казаков Андрей
 * @date     27.01.19.
 */

#include <fcntl.h>
#include <zconf.h>
#include <stdio.h>

int main()
{
	int   fd  = open("/tmp/fifo_server", O_WRONLY);
	pid_t pid = getpid();
	write(fd, &pid, sizeof(pid));
	printf("my pid %d is sent to server\n", (int)pid);
	close(fd);
}
