/**
 * @author   Казаков Андрей 
 * @date     10.08.18.
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/msg.h>

#define HANDLE_ERROR(X) if(X == -1){ printf("%s(%d)", strerror(errno), errno); return -1; }

struct message
{
	long mtype;
	char mtext[80];
};

/**
 * flags  int32
 * 00000000000000000000000		rwx		rwx		rwx
 * 	флаги						владелец создатель остальные
 * 	IPC_CREAT - создание очереди не обязателен при IPC_PRIVATE
 */

int main()
{
	key_t key = ftok("/tmp/msg.temp", 1);

	HANDLE_ERROR(key);

	int queue_id = msgget(key, IPC_CREAT | 0666);

	HANDLE_ERROR(queue_id);

	struct msqid_ds data;
	bzero(&data, sizeof(data));

	int out = msgctl(queue_id, IPC_STAT, &data);

	HANDLE_ERROR(out);

	struct message msg;
	msg.mtype = 1;

	memcpy(msg.mtext, "Hello", 5);

	out = msgsnd(queue_id, &msg, sizeof(struct message) - sizeof(long), 0);

	HANDLE_ERROR(out);

	printf("Sent: %d\n", out);

	out = (int)msgrcv(queue_id, &msg, sizeof(msg), 1, 0);

	printf("Received(%d): %s\n", out, msg.mtext);

	out = msgctl(queue_id, IPC_RMID, NULL);

	HANDLE_ERROR(out);
}