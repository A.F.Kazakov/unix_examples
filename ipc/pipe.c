/**
 * @author   Казаков Андрей
 * @date     26.01.19.
 */

#include <zconf.h>
#include <wait.h>
#include <malloc.h>
#include <tkPort.h>
#include <sys/time.h>

struct worker
{
	int fd[2];
	int* array;
	size_t size;
	int    id;
};

int cmp(const void* a, const void* b) { return *(int*)a - *(int*)b; }

void read_from_pipe(int fd, char* in, size_t size)
{
	int     total = 0;
	ssize_t rc    = read(fd, in, size);
	while(rc > 0 && size > 0)
	{
		size -= rc;
		in += rc;
		total += rc;
		rc = read(fd, in, size);
	}
}

void sorter(struct worker* worker, const char* filename)
{
	close(worker->fd[0]);
	FILE* file = fopen(filename, "r");
	size_t size     = 0;
	int    capacity = 1024;
	int* array = malloc(capacity * sizeof(size));
	while(fscanf(file, "%d", &array[size]) > 0)
	{
		++size;
		if(size == capacity)
		{
			capacity *= 2;
			array = realloc(array, capacity * sizeof(size));
		}
	}
	qsort(array, size, sizeof(size), cmp);
	fclose(file);
	printf("Worker %d sorted %d numbers\n", worker->id, (int)size);
	write(worker->fd[1], &size, sizeof(size));
	write(worker->fd[1], array, sizeof(size) * size);
	close(worker->fd[1]);
	free(array);
}

int main(int argc, const char** argv)
{
	struct timeval start;
	gettimeofday(&start, NULL);
	int nfiles = argc - 1;
	struct worker* workers = malloc(sizeof(struct worker) * nfiles);
	struct worker* w       = workers;
	for(int i          = 0; i < nfiles; ++i, ++w)
	{
		pipe(w->fd);
		w->id = i;
		if(fork() == 0)
		{
			sorter(w, argv[i + 1]);
			free(workers);
			return 0;
		}
		close(w->fd[1]);
	}
	int     total_size = 0;
	w = workers;
	for(int i = 0; i < nfiles; ++i, ++w)
	{
		read_from_pipe(w->fd[0], (char*)&w->size, sizeof(w->size));
		w->array = malloc(w->size * sizeof(int));
		read_from_pipe(w->fd[0], (char*)w->array, w->size * sizeof(int));
		printf("Got %d numbers from worker %d\n", (int)w->size, w->id);
		close(w->fd[0]);
		wait(NULL);
		total_size += w->size;
	}
}
