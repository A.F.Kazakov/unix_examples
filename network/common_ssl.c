#include "common_ssl.h"

#include <openssl/err.h>

void init_openssl()
{
	SSL_load_error_strings();
	OpenSSL_add_ssl_algorithms();
}

void print_ssl_error()
{
	ERR_print_errors_fp(stderr);
}

SSL_CTX* create_context(const SSL_METHOD* method, const char* crt, const char* key, const char* ca)
{
	SSL_CTX            * ctx = SSL_CTX_new(method);

	if(ctx == NULL)
	{
		ERR_print_errors_fp(stderr);
		abort();
	}

	SSL_CTX_set_ecdh_auto(ctx, 1);

	if(SSL_CTX_use_certificate_file(ctx, crt, SSL_FILETYPE_PEM) <= 0)
	{
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}

	if(SSL_CTX_use_PrivateKey_file(ctx, key, SSL_FILETYPE_PEM) <= 0)
	{
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}

	if(!SSL_CTX_check_private_key(ctx))
	{
		fprintf(stderr, "Private key does not match the public certificate\n");
		exit(EXIT_FAILURE);
	}

	if(!SSL_CTX_load_verify_locations(ctx, ca, NULL))
	{
		fprintf(stderr, "Private key does not match the public certificate\n");
		exit(EXIT_FAILURE);
	}

	STACK_OF(X509_NAME)* list = SSL_load_client_CA_file(ca);
	if(list == NULL)
	{
		fprintf(stderr, "Certificate validation failed\n");
		exit(EXIT_FAILURE);
	}
	SSL_CTX_set_client_CA_list(ctx, list);
	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER, NULL);

	return ctx;
}

void print_certificates(SSL* ssl)
{
	X509* cert = SSL_get_peer_certificate(ssl);

	if(cert != NULL)
	{
		printf("Certificates information:\n");

		char* line = X509_NAME_oneline(X509_get_subject_name(cert), NULL, 0);
		printf("Subject: %s\n", line);
		free(line);

		line = X509_NAME_oneline(X509_get_issuer_name(cert), NULL, 0);
		printf("Issuer: %s\n", line);
		free(line);

		X509_free(cert);
	}
	else
		fprintf(stderr, "No certificates found.\n");
}
