#include "common.h"

#include <errno.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>

void handle_error(int err)
{
	if(err >= 0)
		return;

	printf("Socket error: %s(%d)", strerror(errno), errno);
	exit(1);
}

int create_listener_socket(unsigned short port)
{
	int listener = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	handle_error(listener);

	static struct sockaddr_in address;
	bzero(&address, sizeof(address));

	address.sin_family      = AF_INET;
	address.sin_port        = htons(port);
	address.sin_addr.s_addr = htonl(INADDR_ANY);

	int out = bind(listener, (struct sockaddr*)&address, sizeof(address));

	handle_error(out);

	out = fcntl(listener, F_GETFL, 0);

	handle_error(out);

	out = fcntl(listener, F_SETFL, out | O_NONBLOCK | SO_REUSEADDR | SO_REUSEPORT);

	handle_error(out);

	out = listen(listener, SOMAXCONN);

	handle_error(out);

	return listener;
}

int create_socket(const char* hostname, unsigned short port)
{
	int sd;
	struct hostent* host;

	if((host = gethostbyname(hostname)) == NULL)
	{
		perror(hostname);
		exit(1);
	}

	sd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

	struct sockaddr_in address;
	bzero(&address, sizeof(address));

	address.sin_family      = AF_INET;
	address.sin_port        = htons(port);
	address.sin_addr.s_addr = (in_addr_t)(*(long*)(host->h_addr));

	if(connect(sd, (struct sockaddr*)&address, sizeof(address)) != 0)
	{
		close(sd);
		perror(hostname);
		exit(1);
	}
	return sd;
}

ssize_t read_from_socket(int descriptor, char* buffer, size_t size)
{
	return recv(descriptor, buffer, size, MSG_NOSIGNAL);
}

ssize_t write_to_socket(int descriptor, char* buffer, size_t size)
{
	return send(descriptor, buffer, size, MSG_NOSIGNAL);
}

int is_disconnected(int out) { return (out == 0 && errno != EAGAIN); }

void cleanup_socket(int descriptor)
{
	shutdown(descriptor, SHUT_RDWR);
	close(descriptor);
}

int accept_socket(int descriptor, char* buffer, int buffer_length)
{
	static struct sockaddr_in address;
	static socklen_t          address_length = sizeof(address);

	bzero(&address, sizeof(address));
	int sock = accept(descriptor, (struct sockaddr*)&address, &address_length);

	handle_error(sock);

	int out = fcntl(sock, F_GETFL, 0);

	handle_error(out);

	out = fcntl(sock, F_SETFL, out | O_NONBLOCK);

	handle_error(out);

	sprintf(buffer, "%s:%d", inet_ntoa(address.sin_addr), ntohs(address.sin_port));

	return sock;
}
