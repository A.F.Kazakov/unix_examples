/**
 * @author   Казаков Андрей 
 * @date     13.08.18.
 */

#ifndef UNIX_EXAMPLES_COMMON_H
#define UNIX_EXAMPLES_COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void handle_error(int);

int create_listener_socket(unsigned short);

int create_socket(const char*, unsigned short);

ssize_t read_from_socket(int, char*, size_t);

ssize_t write_to_socket(int, char*, size_t);

int is_disconnected(int);

void cleanup_socket(int);

int accept_socket(int, char*, int);

#endif //UNIX_EXAMPLES_COMMON_H
