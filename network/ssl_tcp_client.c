/**
 * @author   Казаков Андрей
 * @date     01.02.18.
 */

#include "common.h"
#include "common_ssl.h"

int main(int argc, char** argv, char** env)
{
	if(argc != 6)
	{
		printf("usage: %s <hostname> <port> <crt> <key> <CA>\n", argv[0]);
		exit(0);
	}

	init_openssl();

	SSL_CTX* ctx = create_context(SSLv23_method(), argv[2], argv[3], argv[4]);

	int server = create_socket(argv[1], (uint16_t)strtol(argv[2], NULL, 0));
	SSL* ssl = SSL_new(ctx);
	SSL_set_fd(ssl, server);

	char buf[1024];

	int out = SSL_connect(ssl);
	if(out <= 0)
	{
		print_ssl_error();
		exit(1);
	}

	if(SSL_get_verify_result(ssl) != X509_V_OK)
	{
		printf("Certificate verification error: %i\n", (int)SSL_get_verify_result(ssl));
		exit(0);
	}

	printf("\n\nConnected with %s encryption\n", SSL_get_cipher(ssl));
	print_certificates(ssl);

	scanf("%s", buf);

	out = SSL_write(ssl, buf, sizeof(buf));

	if(out <= 0)
	{
		print_ssl_error();

		exit(0);
	}

	out = SSL_read(ssl, buf, sizeof(buf) - 1);

	if(out <= 0)
	{
		print_ssl_error();
		exit(0);
	}

	buf[out] = '\0';
	printf("Received: \"%s\"\n", buf);
	SSL_free(ssl);

	cleanup_socket(server);
	SSL_CTX_free(ctx);
}
