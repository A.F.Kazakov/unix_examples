/**
 * @author   Казаков Андрей 
 * @date     24.03.18.
 */

#include "common.h"

#include <poll.h>

#define POLL_SIZE 2048

int main(int argc, char** argv)
{
	if(argc < 2)
	{
		printf("Usage: %s <port>\n", argv[0]);
		return 0;
	}

	printf("Started on port %s\n", argv[1]);

	int listener = create_listener_socket((unsigned short)strtol(argv[1], NULL, 0));

	struct pollfd set[POLL_SIZE];
	bzero(&set, sizeof(set));

	set[0].fd     = listener;
	set[0].events = POLLIN;

	int out = 0, actualAmount = 1;

	while(1)
	{
		out = poll(set, (nfds_t)actualAmount, -1);

		handle_error(out);

		static char buff[1024];

		for(int i = 0; i < actualAmount && out; ++i)
			if(set[i].fd && (set[i].revents & POLLIN))
			{
				if(i)
				{
					ssize_t received = read_from_socket(set[i].fd, buff, 1024);

					handle_error((int)received);

					if(is_disconnected(set[i].fd))
					{
						cleanup_socket(set[i].fd);

						set[i].fd = 0;

						printf("Disconnected\n");
					}
					else
					{
						ssize_t sent = write_to_socket(set[i].fd, buff, (size_t)received);
						handle_error((int)sent);
					}
				}
				else
				{
					int sock = accept_socket(listener);

					printf("New connection\n");

					int j = 0;
					while(1)
					{
						if(!set[j].fd)
						{
							set[j].fd     = sock;
							set[j].events = POLLIN;
							break;
						}
						++j;
					}
					++actualAmount;
				}
			}
	}
}
