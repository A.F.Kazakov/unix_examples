/**
 * @author   Казаков Андрей 
 * @date     24.03.18.
 */

#include "common.h"

#include <sys/epoll.h>

#define MAX_EVENTS 2048

int main(int argc, char** argv)
{
	if(argc < 2)
	{
		printf("Usage: %s <port>\n", argv[0]);
		return 0;
	}

	printf("Started on port %s\n", argv[1]);

	int listener = create_listener_socket((unsigned short)strtol(argv[1], NULL, 0));

	int epoll = epoll_create1(0);

	handle_error(epoll);

	struct epoll_event event;
	bzero(&event, sizeof(event));

	event.data.fd = listener;
	event.events  = EPOLLIN;

	int out = epoll_ctl(epoll, EPOLL_CTL_ADD, listener, &event);

	handle_error(out);

	struct epoll_event events[MAX_EVENTS];

	char address_buffer[32];

	while(1)
	{
		int eventsCount = epoll_wait(epoll, events, MAX_EVENTS, -1);

		handle_error(eventsCount);

		for(int i = 0; i < eventsCount; ++i)
			if(events[i].data.fd == listener)
			{
				int sock = accept_socket(listener, address_buffer, 32);

				event.data.fd = sock;
				out = epoll_ctl(epoll, EPOLL_CTL_ADD, sock, &event);

				handle_error(out);

				printf("New connection from %s\n", address_buffer);
			}
			else
			{
				static char buff[1024];

				ssize_t received = read_from_socket(events[i].data.fd, buff, 1024);

				handle_error((int)received);

				if(is_disconnected((int)received))
				{
					cleanup_socket(events[i].data.fd);
					printf("Connection closed\n");
				}
				else
				{
					ssize_t sent = write_to_socket(events[i].data.fd, buff, (size_t)received);
					handle_error((int)sent);
				}
			}
	}
}
