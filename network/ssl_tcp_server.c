/**
 * @author   Казаков Андрей
 * @date     03.02.18.
 */

#include "common.h"
#include "common_ssl.h"

#include <sys/epoll.h>
#include <signal.h>
#include <openssl/rand.h>

#define BUFFER_SIZE          (1<<16)
#define COOKIE_SECRET_LENGTH 16

#define MAX_EVENTS 2048


int    * socket_ptr      = NULL;
SSL_CTX* SSL_context_ptr = NULL;

struct client_data
{
	int descriptor;
	SSL* ssl;
};

void signal_handler(int sig)
{
	printf("Stopping server\n");
	cleanup_socket(*socket_ptr);
	SSL_CTX_free(SSL_context_ptr);
}

void cleanup_client(struct client_data* ptr)
{
	SSL_free(ptr->ssl);
	cleanup_socket(ptr->descriptor);
	free(ptr);

	printf("Connection closed\n");
}

int dtls_verify_callback(int ok, X509_STORE_CTX* ctx) { return 1; }

int generate_cookie(SSL* ssl, unsigned char* cookie, unsigned int* cookie_len)
{
<<<<<<< HEAD
	if(argc != 5)
=======
	unsigned char* buffer, result[EVP_MAX_MD_SIZE];
	unsigned int           length = 0, resultlength;

	struct sockaddr_in peer;

	/* Initialize a random secret */
	if(!cookie_initialized)
>>>>>>> 80952a5... Add dtls server example
	{
		if(!RAND_bytes(cookie_secret, COOKIE_SECRET_LENGTH))
		{
			printf("error setting random cookie secret\n");
			return 0;
		}
		cookie_initialized = 1;
	}

	/* Read peer information */
	(void)BIO_dgram_get_peer(SSL_get_rbio(ssl), &peer);

	/* Create buffer with peer's address and port */
	length = sizeof(struct in_addr);
	length += sizeof(in_port_t);
	buffer = (unsigned char*)OPENSSL_malloc(length);

	if(buffer == NULL)
	{
		printf("out of memory\n");
		return 0;
	}

	memcpy(buffer, &peer.sin_port, sizeof(in_port_t));
	memcpy(buffer + sizeof(peer.sin_port), &peer.sin_addr, sizeof(struct in_addr));

	<<<<<<< HEAD int listener = create_listener_socket((uint16_t)strtol(argv[1], NULL, 0));

	socket_ptr      = &listener;
	SSL_context_ptr = ctx;

	int epoll = epoll_create1(0);

	handle_error(epoll);

	struct epoll_event event;
	bzero(&event, sizeof(event));
	=======
	<<<<<<< HEAD int sock = create_listener_socket((uint16_t)strtol(argv[1], NULL, 0));
	>>>>>>> aac6cb1... Add
	dtls
	server
	example

	event.data.fd = listener;
	event.events  = EPOLLIN;

	<<<<<<< HEAD int out = epoll_ctl(epoll, EPOLL_CTL_ADD, listener, &event);

	handle_error(out);

	struct epoll_event events[MAX_EVENTS];
	=======
	char buf[1024];
	=======
	>>>>>>> aac6cb1... Add
	dtls
	server
	example

	/* Calculate HMAC of buffer using the secret */
	HMAC(EVP_sha1(),
		  (const void*)cookie_secret,
		  COOKIE_SECRET_LENGTH,
		  (const unsigned char*)buffer,
		  length,
		  result,
		  &resultlength);
	OPENSSL_free(buffer);

	memcpy(cookie, result, resultlength);
	*cookie_len = resultlength;
	>>>>>>> e3cd4f8... Add
	dtls
	server
	example

	return 1;
}

int verify_cookie(SSL* ssl, const unsigned char* cookie, unsigned int cookie_len)
{
	unsigned char* buffer, result[EVP_MAX_MD_SIZE];
	unsigned int           length = 0, resultlength;

	struct sockaddr_in peer;

	/* If secret isn't initialized yet, the cookie can't be valid */
	if(!cookie_initialized)
		return 0;

	/* Read peer information */
	(void)BIO_dgram_get_peer(SSL_get_rbio(ssl), &peer);

	/* Create buffer with peer's address and port */
	length = 0;
	length += sizeof(struct in_addr);
	length += sizeof(in_port_t);
	buffer = (unsigned char*)OPENSSL_malloc(length);

	if(buffer == NULL)
	{
		printf("out of memory\n");
		return 0;
	}

	memcpy(buffer, &peer.sin_port, sizeof(in_port_t));
	memcpy(buffer + sizeof(in_port_t), &peer.sin_addr, sizeof(struct in_addr));

	/* Calculate HMAC of buffer using the secret */
	HMAC(EVP_sha1(),
		  (const void*)cookie_secret,
		  COOKIE_SECRET_LENGTH,
		  (const unsigned char*)buffer,
		  length,
		  result,
		  &resultlength);
	OPENSSL_free(buffer);

	if(cookie_len == resultlength && memcmp(result, cookie, resultlength) == 0)
		return 1;

	return 0;
}

struct pass_info
{
	struct sockaddr_in server_addr, client_addr;
	SSL* ssl;
};

int handle_socket_error()
{
	switch(errno)
	{
		case EINTR: printf("Interrupted system call!\n");
			return 1;
		case EBADF: printf("Invalid socket!\n");
			return 0;
#ifdef EHOSTDOWN
		case EHOSTDOWN: printf("Host is down!\n");
			return 1;
#endif
#ifdef ECONNRESET
		case ECONNRESET: printf("Connection reset by peer!\n");
			return 1;
#endif
		case ENOMEM: printf("Out of memory!\n");
			return 0;
		case EACCES: printf("Permission denied!\n");
			return 1;
		default: printf("Unexpected error! (errno = %d)\n", errno);
			return 0;
	}
}

void* connection_handle(void* info)
{
	ssize_t len;
	char    buf[BUFFER_SIZE];
	char    addrbuf[INET6_ADDRSTRLEN];
	struct pass_info* pinfo     = (struct pass_info*)info;
	SSL             * ssl       = pinfo->ssl;
	int            fd, reading  = 0, ret;
	const int      on           = 1, off = 0;
	struct timeval timeout;
	int            num_timeouts = 0, max_timeouts = 5;

	OPENSSL_assert(pinfo->client_addr.sin_family == pinfo->server_addr.sin_family);
	fd = socket(pinfo->client_addr.sin_family, SOCK_DGRAM, 0);
	if(fd < 0)
	{
		<<<<<<< HEAD
		int eventsCount = epoll_wait(epoll, events, MAX_EVENTS, -1);

		handle_error(eventsCount);

		for(int i = 0; i < eventsCount; ++i)
			if(events[i].data.fd == listener)
			{
				static char address_buffer[32];
				int         sock = accept_socket(listener, address_buffer, 32);

				SSL* ssl = SSL_new(ctx);
				SSL_set_fd(ssl, sock);
				=======
				perror("socket");
				goto cleanup;
			}

<<<<<<< HEAD
				if(SSL_accept(ssl) <= 0)
					print_ssl_error();

				print_certificates(ssl);

				bzero(&event, sizeof(event));

				struct client_data* ptr = malloc(sizeof(struct client_data));;

				ptr->descriptor = sock;
				ptr->ssl        = ssl;

				event.data.ptr = ptr;
				event.events   = EPOLLIN | EPOLLET;

				out = epoll_ctl(epoll, EPOLL_CTL_ADD, sock, &event);

				handle_error(out);

				printf("New connection from %s\n", address_buffer);
=======
		setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&on, (socklen_t)
		sizeof(on));
		setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, (const void*)&on, (socklen_t)
		sizeof(on));

		switch(pinfo->client_addr.sin_family)
		{
			case AF_INET:
				if(bind(fd, (const struct sockaddr*)&pinfo->server_addr, sizeof(struct sockaddr_in)))
				{
					perror("bind");
					goto cleanup;
				}
				if(connect(fd, (struct sockaddr*)&pinfo->client_addr, sizeof(struct sockaddr_in)))
				{
					perror("connect");
					goto cleanup;
				}
				break;
			case AF_INET6: setsockopt(fd, IPPROTO_IPV6, IPV6_V6ONLY, (char*)&off, sizeof(off));
				if(bind(fd, (const struct sockaddr*)&pinfo->server_addr, sizeof(struct sockaddr_in6)))
				{
					perror("bind");
					goto cleanup;
				}
				if(connect(fd, (struct sockaddr*)&pinfo->client_addr, sizeof(struct sockaddr_in6)))
				{
					perror("connect");
					goto cleanup;
				}
				break;
			default: OPENSSL_assert(0);
				break;
		}

		/* Set new fd and set BIO to connected */
		BIO_set_fd(SSL_get_rbio(ssl), fd, BIO_NOCLOSE);
		BIO_ctrl(SSL_get_rbio(ssl), BIO_CTRL_DGRAM_SET_CONNECTED, 0, &pinfo->client_addr);

		/* Finish handshake */
		do
		{
			ret = SSL_accept(ssl);
		}
		while(ret == 0);
		if(ret < 0)
		{
			perror("SSL_accept");
			printf("%s\n", ERR_error_string(ERR_get_error(), buf));
			goto cleanup;
		}

		/* Set and activate timeouts */
		timeout.tv_sec  = 5;
		timeout.tv_usec = 0;
		BIO_ctrl(SSL_get_rbio(ssl), BIO_CTRL_DGRAM_SET_RECV_TIMEOUT, 0, &timeout);

		{
			<<<<<<< HEAD int client = accept_socket(sock);
			>>>>>>> aac6cb1... Add
			dtls
			server
			example

			if(SSL_accept(ssl) <= 0)
				print_ssl_error();

			<<<<<<< HEAD print_certificates(ssl);

			event.data.fd  = sock;
			event.data.ptr = ssl;

			out = epoll_ctl(epoll, EPOLL_CTL_ADD, sock, &event);
			=======
//		printf("Connection: %s:%d\n", inet_ntoa(new_address.sin_addr), ntohs(new_address.sin_port));
			=======
			if(pinfo->client_addr.sin_family == AF_INET)
			{
				printf("\nThread %lx: accepted connection from %s:%d\n",
						 0,
						 inet_ntop(AF_INET, &pinfo->client_addr.sin_addr, addrbuf, INET6_ADDRSTRLEN),
						 ntohs(pinfo->client_addr.sin_port));
>>>>>>> 80952a5... Add dtls server example
			}
		}

		if(SSL_get_peer_certificate(ssl))
		{
			printf("------------------------------------------------------------\n");
			X509_NAME_print_ex_fp(stdout, X509_get_subject_name(SSL_get_peer_certificate(ssl)), 1, XN_FLAG_MULTILINE);
			printf("\n\n Cipher: %s", SSL_CIPHER_get_name(SSL_get_current_cipher(ssl)));
			printf("\n------------------------------------------------------------\n\n");
		}
		>>>>>>> e3cd4f8... Add
		dtls
		server
		example

		while(!(SSL_get_shutdown(ssl) & SSL_RECEIVED_SHUTDOWN) && num_timeouts < max_timeouts)
		{

			<<<<<<< HEAD
			if(SSL_accept(ssl) <= 0)
				print_ssl_error();
			>>>>>>> aac6cb1... Add
			dtls server
			example

			handle_error(out);

			printf("New connection\n");
		}
			else
			{
				static char buff[1024];

				struct client_data* client = events[i].data.ptr;

				out = SSL_read(client->ssl, buff, sizeof(buff) - 1);

				if(out <= 0)
				{
					print_ssl_error();
					out = epoll_ctl(epoll, EPOLL_CTL_DEL, client->descriptor, NULL);
					handle_error(out);

					cleanup_client(client);
					continue;
				}

				<<<<<<< HEAD out = SSL_write(events[i].data.ptr, buff, out);

				if(out <= 0)
				{
					print_ssl_error();
					out = epoll_ctl(epoll, EPOLL_CTL_DEL, client->descriptor, NULL);
					handle_error(out);

					cleanup_client(client);
					continue;
				}
			}
	}
<<<<<<< HEAD
=======

//		SSL_free(ssl);
//		cleanup_socket(client);
//	}
//
//	cleanup_socket(sock);
//	SSL_CTX_free(ctx);
	=======
	if(out <= 0)
	{
		print_ssl_error();
		continue;
		=======
		reading = 1;
		while(reading)
		{
			len = SSL_read(ssl, buf, sizeof(buf));

			switch(SSL_get_error(ssl, len))
			{
				case SSL_ERROR_NONE: printf("Thread %lx: read %d bytes\n", 0, (int)len);
					reading = 0;
					break;
				case SSL_ERROR_WANT_READ:
					/* Handle socket timeouts */
					if(BIO_ctrl(SSL_get_rbio(ssl), BIO_CTRL_DGRAM_GET_RECV_TIMER_EXP, 0, NULL))
					{
						num_timeouts++;
						reading = 0;
					}
					/* Just try again */
					break;
				case SSL_ERROR_ZERO_RETURN: reading = 0;
					break;
				case SSL_ERROR_SYSCALL: printf("Socket read error: ");
					if(!handle_socket_error())
						goto cleanup;
					reading = 0;
					break;
				case SSL_ERROR_SSL: printf("SSL read error: ");
					printf("%s (%d)\n", ERR_error_string(ERR_get_error(), buf), SSL_get_error(ssl, len));
					goto cleanup;
				default: printf("Unexpected error while reading!\n");
					goto cleanup;
			}
		}

		if(len > 0)
		{
			len = SSL_write(ssl, buf, len);

			switch(SSL_get_error(ssl, len))
			{
				case SSL_ERROR_NONE: printf("Thread %lx: wrote %d bytes\n", 0, (int)len);
					break;
				case SSL_ERROR_WANT_WRITE:
					/* Can't write because of a renegotiation, so
					 * we actually have to retry sending this message...
					 */
					break;
				case SSL_ERROR_WANT_READ:
					/* continue with reading */
					break;
				case SSL_ERROR_SYSCALL: printf("Socket write error: ");
					if(!handle_socket_error())
						goto cleanup;
					//reading = 0;
					break;
				case SSL_ERROR_SSL: printf("SSL write error: ");
					printf("%s (%d)\n", ERR_error_string(ERR_get_error(), buf), SSL_get_error(ssl, len));
					goto cleanup;
				default: printf("Unexpected error while writing!\n");
					goto cleanup;
			}
			>>>>>>> e3cd4f8... Add
			dtls
			server example
		}
	}

	SSL_shutdown(ssl);

	cleanup:
	close(fd);
	free(info);
	SSL_free(ssl);
	printf("Thread %lx: done, connection closed.\n", 0);
	return 0;
}

<<<<<<<
HEAD SSL_free(ssl);
cleanup_socket(client);
}

cleanup_socket(sock);
SSL_CTX_free  (ctx);
=======
void start_server(int port, char* local_address)
{
	int                fd;
	struct sockaddr_in server_addr, client_addr;

	pthread_t tid;
	SSL_CTX* ctx;
	SSL    * ssl;
	BIO    * bio;
	struct timeval timeout;
	struct pass_info* info;
	const int on = 1, off = 0;

	memset(&server_addr, 0, sizeof(server_addr));

	if(inet_pton(AF_INET, local_address, &server_addr.sin_addr) == 1)
	{
		printf("Fuck\n");
		exit(3);
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_port   = htons(port);

//	THREAD_setup();
	OpenSSL_add_ssl_algorithms();
	SSL_load_error_strings();
	ctx = SSL_CTX_new(DTLS_server_method());

	SSL_CTX_set_session_cache_mode(ctx, SSL_SESS_CACHE_OFF);

	if(!SSL_CTX_use_certificate_file(ctx, "server.crt", SSL_FILETYPE_PEM))
		printf("\nERROR: no certificate found!");

	if(!SSL_CTX_use_PrivateKey_file(ctx, "server.key", SSL_FILETYPE_PEM))
		printf("\nERROR: no private key found!");

	if(!SSL_CTX_check_private_key(ctx))
		printf("\nERROR: invalid private key!");

	/* Client has to authenticate */
	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_CLIENT_ONCE, dtls_verify_callback);

	SSL_CTX_set_read_ahead(ctx, 1);
	SSL_CTX_set_cookie_generate_cb(ctx, generate_cookie);
	SSL_CTX_set_cookie_verify_cb(ctx, &verify_cookie);

	fd = socket(server_addr.sin_family, SOCK_DGRAM, 0);
	if(fd < 0)
	{
		perror("socket");
		exit(-1);
	}

	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (const void*)&on, (socklen_t)
	sizeof(on));
	setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, (const void*)&on, (socklen_t)
	sizeof(on));

	if(bind(fd, (const struct sockaddr*)&server_addr, sizeof(struct sockaddr_in)))
	{
		perror("bind");
		exit(EXIT_FAILURE);
	}

	while(1)
	{
		memset(&client_addr, 0, sizeof(client_addr));

		/* Create BIO */
		bio = BIO_new_dgram(fd, BIO_NOCLOSE);

		/* Set and activate timeouts */
		timeout.tv_sec  = 5;
		timeout.tv_usec = 0;
		BIO_ctrl(bio, BIO_CTRL_DGRAM_SET_RECV_TIMEOUT, 0, &timeout);

		ssl = SSL_new(ctx);

		SSL_set_bio(ssl, bio, bio);
		SSL_set_options(ssl, SSL_OP_COOKIE_EXCHANGE);

		while(DTLSv1_listen(ssl, (BIO_ADDR*)&client_addr) <= 0);

		info = (struct pass_info*)malloc(sizeof(struct pass_info));
		memcpy(&info->server_addr, &server_addr, sizeof(struct sockaddr_storage));
		memcpy(&info->client_addr, &client_addr, sizeof(struct sockaddr_storage));
		info->ssl = ssl;

//		if(pthread_create(&tid, NULL, connection_handle, info) != 0)
//		{
//			perror("pthread_create");
//			exit(-1);
//		}
		connection_handle(info);
	}

//	THREAD_cleanup();
}

int main(int argc, char** argv)
{
//	if(argc != 4)
//	{
//		printf("Usage: %s <port> <cert> <key> <CA>\n", argv[0]);
//		exit(0);
//	}

	printf("Starting on port %s\n", argv[1]);

	signal(SIGINT, signal_handler);

	start_server(2000, "127.0.0.1");
	>>>>>>> e3cd4f8... Add
	dtls
	server
	example >> >> >> > aac6cb1... Add dtls
	server                            example
>>>>>>> 80952a5... Add dtls server example
}
