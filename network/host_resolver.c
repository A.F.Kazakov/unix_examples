/**
 * @author   Казаков Андрей 
 * @date     19.07.19.
 */

#include <stdio.h>
#include <netdb.h>
#include <arpa/inet.h>

int main(int argc, char** argv)
{
	if(argc < 2)
	{
		printf("Usage: %s hostname", argv[0]);
		return -1;
	}

	struct hostent* hp = gethostbyname(argv[1]);

	if(hp == NULL)
		printf("Error: %s(%d)\n", hstrerror(h_errno), h_errno);
	else
	{
		printf("%s =\n", hp->h_name);
		unsigned int i = 0;
		while(hp->h_addr_list[i] != NULL)
			printf("\t%s\n", inet_ntoa(*(struct in_addr*)(hp->h_addr_list[i++])));
		printf("\n");
	}
}
