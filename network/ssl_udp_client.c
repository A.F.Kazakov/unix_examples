#include "common_ssl.h"
#include <openssl/bio.h>
#include <openssl/rand.h>

#include <signal.h>

#define BUFFER_SIZE          (1<<16)
#define COOKIE_SECRET_LENGTH 16

int* socket_ptr = NULL;

int create_socket(uint16_t port)
{
	int s = socket(AF_INET, SOCK_DGRAM, 0);
	if(s < 0)
	{
		perror(strerror(errno));
		exit(EXIT_FAILURE);
	}

	struct sockaddr_in address;

	address.sin_family      = AF_INET;
	address.sin_port        = htons(port);
	address.sin_addr.s_addr = htonl(INADDR_ANY);

	if(bind(s, (struct sockaddr*)&address, sizeof(address)) < 0)
	{
		perror(strerror(errno));
		exit(EXIT_FAILURE);
	}

	return s;
}

void signal_handler(int sig) { close(*socket_ptr); }

int handle_socket_error()
{
	switch(errno)
	{
		case EINTR: printf("Interrupted system call!\n");
			return 1;
		case EBADF: printf("Invalid socket!\n");
			return 0;
#ifdef EHOSTDOWN
		case EHOSTDOWN: printf("Host is down!\n");
			return 1;
#endif
#ifdef ECONNRESET
		case ECONNRESET: printf("Connection reset by peer!\n");
			return 1;
#endif
		case ENOMEM: printf("Out of memory!\n");
			return 0;
		case EACCES: printf("Permission denied!\n");
			return 1;
		default: printf("Unexpected error! (errno = %d)\n", errno);
			return 0;
	}
}

void start_client(char* remote_address, char* local_address, int port, int length, int messagenumber)
{
	int                fd, retval;
	struct sockaddr_in remote_addr, local_addr;
	char               buf[BUFFER_SIZE];
	char               addrbuf[INET6_ADDRSTRLEN];
	socklen_t          len;
	SSL_CTX* ctx;
	SSL    * ssl;
	BIO    * bio;
	int            reading = 0;
	struct timeval timeout;

	memset((void*)&remote_addr, 0, sizeof(struct sockaddr_in));
	memset((void*)&local_addr, 0, sizeof(struct sockaddr_in));

	if(!inet_pton(AF_INET, remote_address, &remote_addr.sin_addr))
	{
		printf("Fuck!\n");
		exit(2);
	}

	remote_addr.sin_family = AF_INET;
	remote_addr.sin_port   = htons(port);

	fd = socket(remote_addr.sin_family, SOCK_DGRAM, 0);
	if(fd < 0)
	{
		perror("socket");
		exit(-1);
	}

	if(strlen(local_address) > 0)
	{
		local_addr.sin_family = AF_INET;
		local_addr.sin_port   = htons(0);

		OPENSSL_assert(remote_addr.sin_family == local_addr.sin_family);
		if(bind(fd, (const struct sockaddr*)&local_addr, sizeof(struct sockaddr_in)))
		{
			perror("bind");
			exit(EXIT_FAILURE);
		}
	}

	OpenSSL_add_ssl_algorithms();
	SSL_load_error_strings();
	ctx = SSL_CTX_new(DTLS_client_method());

	if(!SSL_CTX_use_certificate_file(ctx, "client.crt", SSL_FILETYPE_PEM))
		printf("\nERROR: no certificate found!");

	if(!SSL_CTX_use_PrivateKey_file(ctx, "client.key", SSL_FILETYPE_PEM))
		printf("\nERROR: no private key found!");

	if(!SSL_CTX_check_private_key(ctx))
		printf("\nERROR: invalid private key!");

	SSL_CTX_set_verify_depth(ctx, 2);
	SSL_CTX_set_read_ahead(ctx, 1);

	ssl = SSL_new(ctx);

	/* Create BIO, connect and set to already connected */
	bio = BIO_new_dgram(fd, BIO_CLOSE);

	if(connect(fd, (struct sockaddr*)&remote_addr, sizeof(struct sockaddr_in)))
	{
		perror("connect");
	}

	BIO_ctrl(bio, BIO_CTRL_DGRAM_SET_CONNECTED, 0, &remote_addr);

	SSL_set_bio(ssl, bio, bio);

	retval = SSL_connect(ssl);
	if(retval <= 0)
	{
		switch(SSL_get_error(ssl, retval))
		{
			case SSL_ERROR_ZERO_RETURN: fprintf(stderr, "SSL_connect failed with SSL_ERROR_ZERO_RETURN\n");
				break;
			case SSL_ERROR_WANT_READ: fprintf(stderr, "SSL_connect failed with SSL_ERROR_WANT_READ\n");
				break;
			case SSL_ERROR_WANT_WRITE: fprintf(stderr, "SSL_connect failed with SSL_ERROR_WANT_WRITE\n");
				break;
			case SSL_ERROR_WANT_CONNECT: fprintf(stderr, "SSL_connect failed with SSL_ERROR_WANT_CONNECT\n");
				break;
			case SSL_ERROR_WANT_ACCEPT: fprintf(stderr, "SSL_connect failed with SSL_ERROR_WANT_ACCEPT\n");
				break;
			case SSL_ERROR_WANT_X509_LOOKUP: fprintf(stderr, "SSL_connect failed with SSL_ERROR_WANT_X509_LOOKUP\n");
				break;
			case SSL_ERROR_SYSCALL: fprintf(stderr, "SSL_connect failed with SSL_ERROR_SYSCALL\n");
				break;
			case SSL_ERROR_SSL: fprintf(stderr, "SSL_connect failed with SSL_ERROR_SSL\n");
				break;
			default: fprintf(stderr, "SSL_connect failed with unknown error\n");
				break;
		}
		exit(EXIT_FAILURE);
	}

	/* Set and activate timeouts */
	timeout.tv_sec  = 3;
	timeout.tv_usec = 0;
	BIO_ctrl(bio, BIO_CTRL_DGRAM_SET_RECV_TIMEOUT, 0, &timeout);

	printf("\nConnected to %s\n", inet_ntop(AF_INET, &remote_addr.sin_addr, addrbuf, INET6_ADDRSTRLEN));

	if(SSL_get_peer_certificate(ssl))
	{
		printf("------------------------------------------------------------\n");
		X509_NAME_print_ex_fp(stdout, X509_get_subject_name(SSL_get_peer_certificate(ssl)), 1, XN_FLAG_MULTILINE);
		printf("\n\n Cipher: %s", SSL_CIPHER_get_name(SSL_get_current_cipher(ssl)));
		printf("\n------------------------------------------------------------\n\n");
	}

	while(!(SSL_get_shutdown(ssl) & SSL_RECEIVED_SHUTDOWN))
	{
		if(messagenumber > 0)
		{
			len = SSL_write(ssl, buf, length);

			switch(SSL_get_error(ssl, len))
			{
				case SSL_ERROR_NONE: printf("wrote %d bytes\n", (int)len);
					messagenumber--;
					break;
				case SSL_ERROR_WANT_WRITE:
					/* Just try again later */
					break;
				case SSL_ERROR_WANT_READ:
					/* continue with reading */
					break;
				case SSL_ERROR_SYSCALL: printf("Socket write error: ");
					if(!handle_socket_error())
						exit(1);
					//reading = 0;
					break;
				case SSL_ERROR_SSL: printf("SSL write error: ");
					printf("%s (%d)\n", ERR_error_string(ERR_get_error(), buf), SSL_get_error(ssl, len));
					exit(1);
				default: printf("Unexpected error while writing!\n");
					exit(1);
			}

			if(messagenumber == 0)
				SSL_shutdown(ssl);
		}

		reading = 1;
		while(reading)
		{
			len = SSL_read(ssl, buf, sizeof(buf));

			switch(SSL_get_error(ssl, len))
			{
				case SSL_ERROR_NONE: printf("read %d bytes\n", (int)len);
					reading = 0;
					break;
				case SSL_ERROR_WANT_READ:
					/* Stop reading on socket timeout, otherwise try again */
					if(BIO_ctrl(SSL_get_rbio(ssl), BIO_CTRL_DGRAM_GET_RECV_TIMER_EXP, 0, NULL))
					{
						printf("Timeout! No response received.\n");
						reading = 0;
					}
					break;
				case SSL_ERROR_ZERO_RETURN: reading = 0;
					break;
				case SSL_ERROR_SYSCALL: printf("Socket read error: ");
					if(!handle_socket_error())
						exit(1);
					reading = 0;
					break;
				case SSL_ERROR_SSL: printf("SSL read error: ");
					printf("%s (%d)\n", ERR_error_string(ERR_get_error(), buf), SSL_get_error(ssl, len));
					exit(1);
				default: printf("Unexpected error while reading!\n");
					exit(1);
			}
		}
	}

	close(fd);
	printf("Connection closed.\n");
}

int main(int argc, char** argv)
{
//	if(argc != 5)
//	{
//		printf("Usage: %s <host ><port> <cert> <key> <CA>\n", argv[0]);
//		exit(0);
//	}

	printf("Starting on port %s\n", argv[1]);

	signal(SIGINT, signal_handler);

	start_client("127.0.0.1", "127.0.0.1", 2000, 100, 5);
}
