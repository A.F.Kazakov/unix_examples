#include "common_ssl.h"
#include <openssl/bio.h>
#include <openssl/rand.h>

#include <signal.h>

#define BUFFER_SIZE          (1<<16)
#define COOKIE_SECRET_LENGTH 16

int* socket_ptr                  = NULL;
unsigned char cookie_secret[COOKIE_SECRET_LENGTH];
int           cookie_initialized = 0;

int create_socket(uint16_t port)
{
	int s = socket(AF_INET, SOCK_DGRAM, 0);
	if(s < 0)
	{
		perror(strerror(errno));
		exit(EXIT_FAILURE);
	}

	struct sockaddr_in address;

	address.sin_family      = AF_INET;
	address.sin_port        = htons(port);
	address.sin_addr.s_addr = htonl(INADDR_ANY);

	if(bind(s, (struct sockaddr*)&address, sizeof(address)) < 0)
	{
		perror(strerror(errno));
		exit(EXIT_FAILURE);
	}

	return s;
}

void signal_handler(int sig) { close(*socket_ptr); }

int dtls_verify_callback(int ok, X509_STORE_CTX* ctx) { return 1; }

int generate_cookie(SSL* ssl, unsigned char* cookie, unsigned int* cookie_len)
{
	unsigned char* buffer, result[EVP_MAX_MD_SIZE];
	unsigned int           length = 0, resultlength;

	struct sockaddr_in peer;

	/* Initialize a random secret */
	if(!cookie_initialized)
	{
		if(!RAND_bytes(cookie_secret, COOKIE_SECRET_LENGTH))
		{
			printf("error setting random cookie secret\n");
			return 0;
		}
		cookie_initialized = 1;
	}

	/* Read peer information */
	(void)BIO_dgram_get_peer(SSL_get_rbio(ssl), &peer);

	/* Create buffer with peer's address and port */
	length = sizeof(struct in_addr);
	length += sizeof(in_port_t);
	buffer = (unsigned char*)OPENSSL_malloc(length);

	if(buffer == NULL)
	{
		printf("out of memory\n");
		return 0;
	}

	memcpy(buffer, &peer.sin_port, sizeof(in_port_t));
	memcpy(buffer + sizeof(peer.sin_port), &peer.sin_addr, sizeof(struct in_addr));


	/* Calculate HMAC of buffer using the secret */
	HMAC(EVP_sha1(),
		  (const void*)cookie_secret,
		  COOKIE_SECRET_LENGTH,
		  (const unsigned char*)buffer,
		  length,
		  result,
		  &resultlength);
	OPENSSL_free(buffer);

	memcpy(cookie, result, resultlength);
	*cookie_len = resultlength;

	return 1;
}

int verify_cookie(SSL* ssl, const unsigned char* cookie, unsigned int cookie_len)
{
	unsigned char* buffer, result[EVP_MAX_MD_SIZE];
	unsigned int           length = 0, resultlength;

	struct sockaddr_in peer;

	/* If secret isn't initialized yet, the cookie can't be valid */
	if(!cookie_initialized)
		return 0;

	/* Read peer information */
	(void)BIO_dgram_get_peer(SSL_get_rbio(ssl), &peer);

	/* Create buffer with peer's address and port */
	length = 0;
	length += sizeof(struct in_addr);
	length += sizeof(in_port_t);
	buffer = (unsigned char*)OPENSSL_malloc(length);

	if(buffer == NULL)
	{
		printf("out of memory\n");
		return 0;
	}

	memcpy(buffer, &peer.sin_port, sizeof(in_port_t));
	memcpy(buffer + sizeof(in_port_t), &peer.sin_addr, sizeof(struct in_addr));

	/* Calculate HMAC of buffer using the secret */
	HMAC(EVP_sha1(),
		  (const void*)cookie_secret,
		  COOKIE_SECRET_LENGTH,
		  (const unsigned char*)buffer,
		  length,
		  result,
		  &resultlength);
	OPENSSL_free(buffer);

	if(cookie_len == resultlength && memcmp(result, cookie, resultlength) == 0)
		return 1;

	return 0;
}

int main(int argc, char** argv)
{
	if(argc != 5)
	{
		printf("Usage: %s <port> <cert> <key> <CA>\n", argv[0]);
		exit(0);
	}

	printf("Starting on port %s\n", argv[1]);

	signal(SIGINT, signal_handler);

	init_openssl();

	SSL_CTX* ctx = create_context(DTLS_server_method(), argv[2], argv[3], argv[4]);

	SSL_CTX_set_session_cache_mode(ctx, SSL_SESS_CACHE_OFF);
	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_CLIENT_ONCE, dtls_verify_callback);

	SSL_CTX_set_read_ahead(ctx, 1);
	SSL_CTX_set_cookie_generate_cb(ctx, generate_cookie);
	SSL_CTX_set_cookie_verify_cb(ctx, &verify_cookie);

	int sock = create_socket((uint16_t)strtol(argv[1], NULL, 0));

	int on = 1;

	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (const void*)&on, (socklen_t)
	sizeof(on));
	setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, (const void*)&on, (socklen_t)
	sizeof(on));

	socket_ptr             = &sock;

	struct sockaddr_in new_address;
	uint               len = sizeof(new_address);
	char               buf[1024];

	printf("Listening\n");

	while(1)
	{
		BIO* bio = BIO_new_dgram(sock, BIO_NOCLOSE);

		struct timeval timeout;

		timeout.tv_sec  = 5;
		timeout.tv_usec = 0;
		BIO_ctrl(bio, BIO_CTRL_DGRAM_SET_RECV_TIMEOUT, 0, &timeout);

		SSL* ssl = SSL_new(ctx);

		SSL_set_options(ssl, SSL_OP_COOKIE_EXCHANGE);

		SSL_set_bio(ssl, bio, bio);
		SSL_set_options(ssl, SSL_OP_COOKIE_EXCHANGE);

		while(DTLSv1_listen(ssl, (BIO_ADDR*)&new_address) <= 0);

		printf("Connection: %s:%d\n", inet_ntoa(new_address.sin_addr), ntohs(new_address.sin_port));

		int fd = socket(new_address.sin_family, SOCK_DGRAM, 0);
		if(fd < 0)
		{
			perror("socket");
			exit(1);
		}

		setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (const void*)&on, (socklen_t)
		sizeof(on));
		setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, (const void*)&on, (socklen_t)
		sizeof(on));

//		bind(fd, (const struct sockaddr *) &new_address, sizeof(struct sockaddr_in));
		connect(fd, (struct sockaddr*)&new_address, sizeof(struct sockaddr_in));

		BIO_set_fd(SSL_get_rbio(ssl), fd, BIO_NOCLOSE);
		BIO_ctrl(SSL_get_rbio(ssl), BIO_CTRL_DGRAM_SET_CONNECTED, 0, &new_address);

		int ret = 0;

		do
		{
			ret = SSL_accept(ssl);
		}
		while(ret == 0);

		if(ret < 0)
		{
			perror("SSL_accept");
			ERR_print_errors_fp(stderr);
//			printf("%s\n", ERR_error_string(ERR_get_error(), buf));
			return 0;
		}

		timeout.tv_sec  = 5;
		timeout.tv_usec = 0;
		BIO_ctrl(SSL_get_rbio(ssl), BIO_CTRL_DGRAM_SET_RECV_TIMEOUT, 0, &timeout);

		char addrbuf[INET6_ADDRSTRLEN];

		printf("\nThread %lx: accepted connection from %s:%d\n",
				 0,
				 inet_ntop(AF_INET, &new_address.sin_addr, addrbuf, INET6_ADDRSTRLEN),
				 ntohs(new_address.sin_port));

		printf("------------------------------------------------------------\n");
		X509_NAME_print_ex_fp(stdout, X509_get_subject_name(SSL_get_peer_certificate(ssl)), 1, XN_FLAG_MULTILINE);
		printf("\n\n Cipher: %s", SSL_CIPHER_get_name(SSL_get_current_cipher(ssl)));
		printf("\n------------------------------------------------------------\n\n");

		int num_timeouts = 0, max_timeouts = 5, reading = 0;

		while(!(SSL_get_shutdown(ssl) & SSL_RECEIVED_SHUTDOWN) && num_timeouts < max_timeouts)
		{

			reading = 1;
			while(reading)
			{
				len = SSL_read(ssl, buf, sizeof(buf));

				switch(SSL_get_error(ssl, len))
				{
					case SSL_ERROR_NONE: printf("Thread %lx: read %d bytes\n", 0, (int)len);
						reading = 0;
						break;
					case SSL_ERROR_WANT_READ:
						/* Handle socket timeouts */
						if(BIO_ctrl(SSL_get_rbio(ssl), BIO_CTRL_DGRAM_GET_RECV_TIMER_EXP, 0, NULL))
						{
							num_timeouts++;
							reading = 0;
						}
						/* Just try again */
						break;
					case SSL_ERROR_ZERO_RETURN: reading = 0;
						break;
					case SSL_ERROR_SYSCALL: printf("Socket read error: ");
						reading = 0;
						break;
					case SSL_ERROR_SSL: printf("SSL read error: ");
						printf("%s (%d)\n", ERR_error_string(ERR_get_error(), buf), SSL_get_error(ssl, len));
						break;
					default: printf("Unexpected error while reading!\n");
						break;
				}
			}

			if(len > 0)
			{
				len = SSL_write(ssl, buf, len);

				switch(SSL_get_error(ssl, len))
				{
					case SSL_ERROR_NONE: printf("Thread %lx: wrote %d bytes\n", 0, (int)len);
						break;
					case SSL_ERROR_WANT_WRITE:
						/* Can't write because of a renegotiation, so
						 * we actually have to retry sending this message...
						 */
						break;
					case SSL_ERROR_WANT_READ:
						/* continue with reading */
						break;
					case SSL_ERROR_SYSCALL: printf("Socket write error: ");
						reading = 0;
						break;
					case SSL_ERROR_SSL: printf("SSL write error: ");
						printf("%s (%d)\n", ERR_error_string(ERR_get_error(), buf), SSL_get_error(ssl, len));
						break;
					default: printf("Unexpected error while writing!\n");
						break;
				}
			}
		}

		SSL_shutdown(ssl);
		SSL_free(ssl);
	}

	close(sock);
	SSL_CTX_free(ctx);
}
