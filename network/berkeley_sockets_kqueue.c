/**
 * @author   Казаков Андрей 
 * @date     24.03.18.
 */

#include "common.h"
#include <sys/event.h>

int main(int argc, char** argv)
{
	if(argc < 2)
	{
		printf("Usage: %s <port>\n", argv[0]);
		return 0;
	}

	int listener = create_listener_socket((unsigned short)strtol(argv[1], NULL, 0));

	int queue = kqueue();

	struct kevent event;
	bzero(&event, sizeof(event));
	EV_SET(&event, listener, EVFILT_READ, EV_ADD, 0, 0, 0);

	out = kevent(queue, &event, 1, NULL, 0, NULL);

	handle_error(out);

	while(1)
	{
		bzero(&event, sizeof(event));
		out = kevent(queue, NULL, 0, &event, 1, NULL);

		handle_error(out);

		if(event.filter == EVFILT_READ)
		{
			if(event.ident == listener)
			{
				static char address_buffer[32];

				int sock = accept_socket(listener, (char*)&address_buffer, 32);

				bzero(&event, sizeof(event));
				EV_SET(&event, sock, EVFILT_READ, EV_ADD, 0, 0, 0);
				out = kevent(queue, &kevent, 1, NULL, 0, NULL);

				handle_error(out);

				printf("New connection from %s\n", address_buffer);
			}
			else
			{
				static char buff[1024];

				ssize_t received = read_from_socket(ev.data.fd, buff, 1024);

				handle_error(received);

				if(is_disconnected(ev.data.fd))
				{
					cleanup_socket(event.ident);
					printf("Connection closed\n");
				}
				else
				{
					ssize_t sent = write_to_socket(ev.data.fd, buff, (size_t)received);
					handle_error(sent);
				}
			}
		}
	}
}