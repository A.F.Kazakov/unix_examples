/**
 * @author   Казаков Андрей
 * @date     27.08.18.
 *
 * openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout server.key -out server.crt
 */

#ifndef UNIX_EXAMPLES_SSLCOMMON_H
#define UNIX_EXAMPLES_SSLCOMMON_H

#include <openssl/ssl.h>

void init_openssl();

void print_ssl_error();

SSL_CTX* create_context(const SSL_METHOD* method, const char* crt, const char* key, const char* ca);

void print_certificates(SSL* ssl);

#endif //UNIX_EXAMPLES_SSLCOMMON_H
