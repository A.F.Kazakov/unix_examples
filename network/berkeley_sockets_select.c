/**
 * @author   Казаков Андрей 
 * @date     24.03.18.
 */

#include "common.h"

#define LISTEN_COUNT 2048

int main(int argc, char** argv)
{
	if(argc < 2)
	{
		printf("Usage: %s <port>\n", argv[0]);
		return 0;
	}

	printf("Started on port %s\n", argv[1]);

	int listener = create_listener_socket((unsigned short)strtol(argv[1], NULL, 0));

	int connections[LISTEN_COUNT];
	bzero(&connections, LISTEN_COUNT);

	int    out = 0, connection_count = 0;
	fd_set set;

	while(1)
	{
		FD_ZERO(&set);
		FD_SET(listener, &set);

		int     max = listener;
		for(int i   = 0; i < connection_count; ++i)
			if(connections[i] > 0)
			{
				if(connections[i] > max)
					max = connections[i];
				FD_SET(connections[i], &set);
			}

		out = select(max + 1, &set, NULL, NULL, NULL);

		handle_error(out);

		if(FD_ISSET(listener, &set))
		{
			static char address_buffer[32];

			int sock = accept_socket(listener, (char*)&address_buffer, 32);

			int i = 0;
			while(1)
			{
				if(connections[i] == 0)
				{
					connections[i] = sock;
					++connection_count;
					break;
				}
				++i;
			}

			printf("New connection from %s\n", address_buffer);
		}
		else
			for(int i = 0; i < connection_count; ++i)
			{
				if(FD_ISSET(connections[i], &set))
				{
					static char buff[1024];

					ssize_t received = read_from_socket(connections[i], buff, 1024);

					if(is_disconnected((int)received))
					{
						cleanup_socket(connections[i]);
						connections[i] = 0;

						printf("Connection closed\n");
					}
					else
						if(received != 0)
						{
							ssize_t sent = write_to_socket(connections[i], buff, 1024);
							handle_error((int)sent);
						}
				}
			}
	}
}